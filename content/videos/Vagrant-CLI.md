---
title: "Werken met de Vagrant Command Line Interface"
date: 2020-10-23T23:49:06+02:00
draft: false
---

In deze video leer je hoe je werkt met Vagrant vanuit de Command-line-interface, maak je kennis met de commando's en leer je hoe snel je dingen voor elkaar krijgt met alleen een terminal.

{{< youtube WsfuDQI_NR8 >}}