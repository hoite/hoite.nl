---
title: "Vagrant Provisioning"
date: 2020-11-10T16:18:07+01:00
draft: false
---

In deze video leer je hoe je door middel van provisioning in Vagrant een geinstalleerde webserver out-of-the-box voorbereid.

{{< youtube 70i9sEVGT5g >}}