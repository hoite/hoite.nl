---
title: "Werken met Vagrant | De basis"
date: 2020-10-23T23:44:03+02:00
draft: false
---


In deze video leer je de basis van het werken met Vagrant, het uitrollen van je eerste geautomatiseerde VM en krijg je een eerste blik op de mooie wereld van DevOps.

{{< youtube SGT7LCd4ZVM >}}
